package pacman.agent;

import java.util.ArrayList;

import pacman.elements.AgentPacman;
import pacman.elements.ActionPacman;
import pacman.elements.StateAgentPacman;
import pacman.elements.StateGamePacman;

//Identique au RandomPacmanAgent à la différence que celui ci ne revient sur ses pas
//que s'il est coincé au fond d'un cul-de-sac

public class NotSoRandomPacmanAgent implements AgentPacman
{

	@Override
	public ActionPacman getAction(StateAgentPacman as,StateGamePacman state) 
	{
		/**
		 *  5 possible actions defined in Maze.java:
		 *  public static int STOP=4;
			public static int NORTH=0;
			public static int SOUTH=1;
			public static int EAST=2;
			public static int WEST=3;
		 */
		int lastMove =as.getLastMovement();
		ArrayList<ActionPacman> aa=new ArrayList<ActionPacman>();
		ArrayList<ActionPacman> ab=new ArrayList<ActionPacman>();
		for(int i=0;i<4;i++)
		{
			if (state.isLegalMove(new ActionPacman(i),as)//si essaie d'aller dans mur, reste sur place
					&& !( (i==lastMove-1 && (i==0 || i==2)) || (i==lastMove+1 && (i==1  || i==3)) ) 
				){ //Ne fait jamais demi-tour
				aa.add(new ActionPacman(i));
			}else if (state.isLegalMove(new ActionPacman(i),as)){
				ab.add(new ActionPacman(i));


//				System.out.print(i);
//				System.out.print(lastMove);
//				System.out.print("\n");
			}
		}
		
		if(aa.size()!=0){
			return(aa.get((int)(Math.random()*aa.size())));
		}else{
//			System.out.print(ab);
//
//			System.out.print(lastMove);
			return(ab.get((int)(Math.random()*ab.size())));
		}
	}

}
