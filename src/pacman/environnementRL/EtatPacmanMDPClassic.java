package pacman.environnementRL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import pacman.elements.MazePacman;
import pacman.elements.StateAgentPacman;
import pacman.elements.StateGamePacman;
import environnement.Etat;
/**
 * Classe pour définir un etat du MDP pour l'environnement pacman avec QLearning tabulaire

 */
public class EtatPacmanMDPClassic implements Etat , Cloneable, Serializable{
    //Rendu serializable pour pouvoir exporter la table de qValeurs 
    //(méthodes saveQValeurs et loadQValeurs de QLearningAgent)

	protected transient StateGamePacman etat;
	protected transient int X, Y;
	protected String vision, foodSense, lastMove;
	
	//MODIFICATIONS DES CLASSES MazePacman (isFood) ET StateGamePacman (détermination 
	//de la victoire)
	public EtatPacmanMDPClassic(StateGamePacman _stategamepacman){
        //On va utiliser les chaînes vision, foodSense et lastMove pour représenter
        //la perception du pacman, le hashcode de la concaténation de ces chaines
        //sera le hashcode de l'instance de cette classe qui sera utilisé pour distinguer les
        //états.
        //Les choix de perception du pacman et d'implémentation pour ce constructeur sont
        //détaillés dans le rapport
		etat = _stategamepacman;
		vision = "\n";
		foodSense = "";
		lastMove = "";
		
		StateAgentPacman pacman = etat.getPacmanState(0);
		X = pacman.getX();
		Y = pacman.getY();
		
		MazePacman maze = etat.getMaze();
		int foodRange = etat.getClosestDot(pacman);
		int visionRange = 2;
		Boolean emptyCorridor = true;
		
		//La double boucle ci-dessous parcourt le champ de vision de pacman : un carré de (2*visionRange+1)² centré sur lui-même
		//La chaine de caractères "vision" représente ce que pacman voit, il n'y a qu'à fournir le hashcode 
		//de cette chaine comme hashcode de l'état.
		int x = X-visionRange;
		int y = Y-visionRange;
		int d;
		while(x<=X+visionRange){
			y = Y-visionRange;
			while(y<=Y+visionRange){
				d = Math.abs(x-X) + Math.abs(y-Y);
//				if(d<=visionRange){
					if(y<0 || y>=maze.getSizeY() || x<0 || x>=maze.getSizeX()){
						vision = vision.concat(" "); //On considère l'extérieur du labyrinthe comme du mur (impossible d'aller dessus)
					}else if(maze.isWall(x, y) && d <= 2){
						vision = vision.concat("%");
					}else{
						Boolean isGhost = false;
						if(d<=2){
							for(int i=0;i<etat.getNumberOfGhosts();i++){
								StateAgentPacman s = etat.getGhostState(i);
								if(s.getX()==x && s.getY()==y){
									isGhost = true;
								}
							}
						}
						
						if(isGhost && (!pacman.isScarred() || pacman.getScarredTimer() <= 3) ){
							vision = vision.concat("G");
							emptyCorridor = false;
						}else if(isGhost && pacman.getScarredTimer() > 3){
							vision = vision.concat("@");
							emptyCorridor = false;
						}else if(d==1 && maze.isFood(x, y)){
							vision = vision.concat(".");
							emptyCorridor = false;
						}else{
							vision = vision.concat(" ");
						}
					}
//				}
				y++;
			}
			vision = vision.concat("_");
			x++;
		}
		int dx,dy;
		Boolean foundFood = false;
		int distanceLook = 1;
		while(!foundFood){
			foundFood = true;
			if(maze.isFood(X, Y+distanceLook) || maze.isCapsule(X, Y+distanceLook)){
				foodSense = "U";
			}else if(maze.isFood(X, Y-distanceLook) || maze.isCapsule(X, Y-distanceLook)){
				foodSense = "D";
			}else if(maze.isFood(X+distanceLook, Y) || maze.isCapsule(X+distanceLook, Y)){
				foodSense = "R";
			}else if(maze.isFood(X-distanceLook, Y) || maze.isCapsule(X-distanceLook, Y)){
				foodSense = "L";
			}else if(distanceLook > maze.getSizeY()+maze.getSizeX()){
				foodSense = "OOO";
			}else{
				for(int i=0;i<=distanceLook;i++){
					dx = i;
					dy = distanceLook-i;
					if(maze.isFood(X+dx, Y+dy) || maze.isCapsule(X+dx, Y+dy)){ //1er quadrant
						foodSense = "1";
					}else if(maze.isFood(X-dx, Y+dy) || maze.isCapsule(X-dx, Y+dy)){ //2eme quadrant
						foodSense = "2";
					}else if(maze.isFood(X-dx, Y-dy) || maze.isCapsule(X-dx, Y-dy)){ //2eme quadrant
						foodSense = "3";
					}else if(maze.isFood(X+dx, Y-dy) || maze.isCapsule(X+dx, Y-dy)){ //2eme quadrant
						foodSense = "4";
					}
				}
			}
			if(foodSense==""){
				foundFood = false;
			}
			distanceLook++;
		}
		
		if(emptyCorridor){
			lastMove = ((Integer)pacman.getLastMovement()).toString();
		}
		//System.out.print(vision);
	}
	
	@Override
	public String toString() {
		
		return vision.concat("_").concat(foodSense).concat("_").concat(lastMove);
	}
	
	
	public Object clone() {
		EtatPacmanMDPClassic clone = null;
		try {
			// On recupere l'instance a renvoyer par l'appel de la 
			// methode super.clone()
			clone = (EtatPacmanMDPClassic)super.clone();
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous implementons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		


		// on renvoie le clone
		return clone;
	}
	
	@Override
	public int hashCode(){
		String hashedString = "";
		hashedString = hashedString.concat(vision).concat(foodSense).concat(lastMove);
//		hashedString = foodSense;
		//System.out.println(hashedString.hashCode());
		return hashedString.hashCode();
	}

	@Override
	public boolean equals(Object e){
		return this.hashCode() == e.hashCode();
	}

}
