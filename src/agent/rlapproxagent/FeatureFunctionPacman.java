package agent.rlapproxagent;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;

import pacman.elements.ActionPacman;
import pacman.elements.StateAgentPacman;
import pacman.elements.StateGamePacman;
import pacman.environnementRL.EnvironnementPacmanMDPClassic;
import pacman.environnementRL.EtatPacmanMDPClassic;
import environnement.Action;
import environnement.Etat;
/**
 * Vecteur de fonctions caracteristiques pour jeu de pacman: 4 fonctions phi_i(s,a)
 *  
 * @author laetitiamatignon
 *
 */
public class FeatureFunctionPacman implements FeatureFunction{
	private double[] vfeatures ;
	private int nFeatures;
	private HashMap<Etat,HashMap<Action,Double>> qvaleurs;
	
	private static int NBACTIONS = 4;//5 avec NONE possible pour pacman, 4 sinon 
	//--> doit etre coherent avec EnvironnementPacmanRL::getActionsPossibles


	public FeatureFunctionPacman(){
		nFeatures = 4;
//		ObjectInputStream objectinputstream = null;
//		try {
//		    FileInputStream streamIn = new FileInputStream("qvaleurs.ser");
//		    objectinputstream = new ObjectInputStream(streamIn);
//		    qvaleurs = (HashMap<Etat,HashMap<Action,Double>>) objectinputstream.readObject();
//		} catch (Exception e) {
//		    e.printStackTrace();
//		} finally {
//		    if(objectinputstream != null){
//		        try {
//					objectinputstream.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		    } 
//		}
	}

	@Override
	public int getFeatureNb() {
		return nFeatures;
	}

	@Override
	public double[] getFeatures(Etat e, Action a) {
		vfeatures = new double[nFeatures];
		StateGamePacman stategamepacman ;
		//EnvironnementPacmanMDPClassic envipacmanmdp = (EnvironnementPacmanMDPClassic) e;

		//calcule pacman resulting position a partir de Etat e
		if (e instanceof StateGamePacman){
			stategamepacman = (StateGamePacman)e;
		}
		else{
			System.out.println("erreur dans FeatureFunctionPacman::getFeatures n'est pas un StateGamePacman");
			return vfeatures;
		}
	
		StateAgentPacman pacmanstate_next= stategamepacman.movePacmanSimu(0, new ActionPacman(a.ordinal()));
		 
		//*** VOTRE CODE
		int pacX = pacmanstate_next.getX();
		int pacY = pacmanstate_next.getY();
		//BIAIS
		vfeatures[0] = 1;
		//FANTOMES
		vfeatures[1] = 0;
		if(stategamepacman.isGhost(pacX+1, pacY)) vfeatures[1]+=1;
		if(stategamepacman.isGhost(pacX-1, pacY)) vfeatures[1]+=1;
		if(stategamepacman.isGhost(pacX, pacY+1)) vfeatures[1]+=1;
		if(stategamepacman.isGhost(pacX, pacY-1)) vfeatures[1]+=1;
		vfeatures[1]/=4;
		//MANGE
		if(stategamepacman.getMaze().isFood(pacX, pacY) || stategamepacman.getMaze().isCapsule(pacX, pacY)) vfeatures[2]=1;
		else vfeatures[2] = 0;
		//DISTANCE AU DOT LE PLUS PROCHE
		vfeatures[3] = (double)stategamepacman.getClosestDot(pacmanstate_next) / (stategamepacman.getMaze().getSizeX()+stategamepacman.getMaze().getSizeY());
		
//		EtatPacmanMDPClassic eMDP = new EtatPacmanMDPClassic(stategamepacman); 
//		if(!this.qvaleurs.containsKey(eMDP) || !this.qvaleurs.get(eMDP).containsKey(a)){
//			vfeatures[4] = 0.0;
//		}else{
//			vfeatures[4] = qvaleurs.get(eMDP).get(a);
//		}
		return vfeatures;
	}

	public void reset() {
		vfeatures = new double[4];
		
	}

}
